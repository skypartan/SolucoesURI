# https://www.urionlinejudge.com.br/judge/pt/problems/view/1022

import re

DEBUG = True


def parse_equations(equations):
    if DEBUG: print(str(equations))

    eqs = []
    for equation in equations:
        operators = re.findall("[/*\-+]", equation)
        values = [int(value) for value in re.findall("[0-9]+", equation)]

        eqs.append((values, operators))

        if DEBUG:
            print("Current equation: " + equation)
            print("Operators: " + str(operators))
            print("Values: " + str(values))
    
    return eqs


def solve_equations(equations):
    solutions = []

    for equation in equations:
        values = equation[0]
        operators = equation[1]

        num = 0
        den = 1

        if (operators[1] == "/"):
            num = values[0] * values[3]
            den = values[1] * values[2]
        if (operators[1] == "*"):
            num = values[0] * values[2]
            den = values[1] * values[3]
        if (operators[1] == "-"):
            num = (values[0] * values[3]) - (values[1] * values[2])
            den = (values[1] * values[3])
        if (operators[1] == "+"):
            num = (values[0] * values[3]) + (values[1] * values[2])
            den = (values[1] * values[3])

        eq = str(num) + "/" + str(den)
            
        div_common = gcd(num, den)
        eq += " = " + str(int(num / div_common)) + "/" + str(int(den / div_common))

        solutions.append(eq)

    return solutions


def gcd(a, b): # TODO: Arender
    while b:
        a, b = b, a % b
    
    return a


equations = []

if DEBUG: print("Write data:")

rawin_linecount = input()
for i in range(int(rawin_linecount)):
    equation_linein = input()
    equations.append(equation_linein)

equations = parse_equations(equations)
solutions = solve_equations(equations)

if DEBUG: print("Solutions: ")

for i in solutions:
    print(i)